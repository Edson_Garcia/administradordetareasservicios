package com.ekt.app.repository;

import com.ekt.app.document.TaskLog;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaskLogRepository extends MongoRepository<TaskLog, String> {
}
