package com.ekt.app.exception;

public class TareaCollectionException extends Exception{

    private static final long serialVersionUID = 1L;

    public TareaCollectionException(String message){
        super(message);
    }

    public static String NotFoundException(String id){
        return "Tarea con " + id + " Not found";
    }

    public static String TareaAlreadyExists(){
        return "Tarea con nombre ya existe ";
    }
}
