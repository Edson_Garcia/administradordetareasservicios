package com.ekt.app.service;

import com.ekt.app.document.Task;
import com.ekt.app.exception.TareaCollectionException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Optional;


public interface TaskService {
    public Iterable<Task> findAll();
    public Page<Task> findAll(Pageable pageable);
    public Optional<Task> findById(String id);
    public Task save(Task tarea);
    public void updateById(String id, Task tarea) throws TareaCollectionException;
    public void  deleteById(String id ) throws TareaCollectionException;
    public void  actualizarEstatus(String id_tarea, String estatus );
    public void actualizaLeido(String id_tarea,Boolean leido);
    public void updateRealDateStart(String id_tarea, Task tarea);
    public void updateRealDateFinish(String id_tarea, Task tarea);
    public void notificacion(String token, String asunto);
    public ArrayList<String> validarTareasCrear(Task tarea);
    public ArrayList<String> validarTareasActualizar(Task tarea);

}
