package com.ekt.app.service;

import com.ekt.app.document.TaskLog;

public interface TaskLogService {

    public TaskLog save(TaskLog bitacora);
}
