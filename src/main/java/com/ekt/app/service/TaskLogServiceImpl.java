package com.ekt.app.service;

import com.ekt.app.document.TaskLog;
import com.ekt.app.repository.TaskLogRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class TaskLogServiceImpl implements TaskLogService {

    @Autowired
    TaskLogRepository bitacoraRepository;

    @Override
    public TaskLog save(TaskLog bitacora) {

        return bitacoraRepository.save(bitacora);
    }
}
