package com.ekt.app.document;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Periodo {
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Field(name = "fecha_ini")
    private Date fecha_ini;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Field(name = "fecha_fin")
    private Date fecha_fin;

    public Periodo() {
    }

    public Periodo(Date fecha_ini, Date fecha_fin) {
        this.fecha_ini = fecha_ini;
        this.fecha_fin = fecha_fin;
    }

    public Date getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(Date fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
}
