package com.ekt.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class AppApplication {
		@PostConstruct
		void started() {
			TimeZone.setDefault(TimeZone.getTimeZone("America/Mexico_City"));
		}

		public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
	}

}
