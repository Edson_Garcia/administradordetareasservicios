package com.ekt.app.controller;

import com.ekt.app.document.TaskLog;
import com.ekt.app.document.ResponseTask;
import com.ekt.app.document.Task;
import com.ekt.app.repository.TaskLogRepository;
import com.ekt.app.repository.TaskRepository;
import com.ekt.app.service.TaskServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@RestController
@RequestMapping("/api/tareas")
public class TaskController {
    @Autowired
    private TaskServiceImp tareaService;

    @Autowired
    private TaskRepository tareaRepository;

    @Autowired
    private TaskLogRepository bitacoraRepository;



    @PostMapping("/agregarTarea")   //1. Tareas
    public ResponseEntity<?> create(@RequestBody Task tarea){
        LocalDateTime date =  LocalDateTime.now();
        //Validar atributos entrantes
        String mensaje = "";

        System.out.println("Entramos en agregar tarea");
                                ArrayList<String> validarTareas = tareaService.validarTareasCrear(tarea);
                                if(validarTareas.size() == 0){
                                    TaskLog bitacora = new TaskLog();
                                    bitacora.setId_emisor(tarea.getId_emisor());
                                    bitacora.setNombre_emisor(tarea.getNombre_emisor());
                                    bitacora.setAccion("Creo una tarea");
                                    bitacora.setFecha_actualizacion(date);
                                    bitacora.setEstatus(tarea.getEstatus());
                                    bitacoraRepository.save(bitacora);
                                    tarea.setFecha_BD(date);
                                    //Usuarios usuario = tareaRepository.obtenerUsuario(tarea.getId_receptor());
                                    //String token = usuario.token;
                                    //tareaService.notificacion(tarea.getToken(), tarea.getTitulo());

                                    LocalDateTime ldt = date
                                            .atZone(ZoneId.systemDefault())
                                            .toLocalDateTime();

                                    /*LocalDateTime ldt = date.toInstant()
                                            .atZone(ZoneId.systemDefault())
                                            .toLocalDateTime();*/

                                     /*
                                     ldt = ldt.minusHours(6);
                                    date = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
                                    tarea.setFecha_BD(date);*/
                                    //ZonedDateTime ldtZoned = ldt.atZone(ZoneId.systemDefault());
                                    /*LocalDateTime newDateTime = ldt.atZone(ZoneId.of("UTC"))
                                            .withZoneSameInstant(ZoneId.systemDefault())
                                            .toLocalDateTime();*/
                                    tarea.setFecha_BD(ldt);

                                    tareaService.save(tarea);

                                    mensaje = "Tarea creada correctamente";
                                    return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()), mensaje, tarea));
                                }else {mensaje = "Error de validacion de tarea";
                                    return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY.value()), mensaje, validarTareas));}
    }

    @GetMapping(value="/obtenerTareaPorId/{id}")    //2. Tareas
    public ResponseEntity<?> read(@PathVariable String id){
        Optional<Task> oTarea = tareaService.findById(id);
        String mensaje;
        //Si el iD no existe

        //Si no hay ninguna tarea
        if(!oTarea.isPresent()){
            Map<String, Object> body = new LinkedHashMap<>();//LinkedHashMap conserva el orden de inserción
            Map<String, String> data = new LinkedHashMap<>();
            body.put("statusCode", String.valueOf(HttpStatus.NOT_FOUND.value()));
            data.put("Tarea","No existe la tarea con Id: "+ id);
            body.put("Data",data);
            mensaje = "No existe tarea con id: "+id;
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
        mensaje = "Se encontró una tarea";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,oTarea));
    }

    @GetMapping("/obtenerTareas")   //Reportes
    public ResponseEntity<?> readAll(){
        Iterable<Task> tareas = tareaService.findAll();
        String mensaje;
        int nDocumentos = ((Collection<Task>) tareas).size();
        if(nDocumentos == 0) {
            mensaje = "No se encontraron tareas";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()), mensaje));
        }
        mensaje = "Tareas encontradas";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,tareas));
    }

    @PutMapping("/actualizarTarea/{id_tarea}")  //3. Tareas
    public ResponseEntity<?> updateById(@PathVariable String id_tarea,@RequestBody Task tarea){
        Optional<Task> oTarea = tareaService.findById(id_tarea);
        String mensaje = "";
        if(oTarea.isPresent()) {
            ArrayList<String> validarActualizacion = tareaService.validarTareasActualizar(tarea);
            if(validarActualizacion.size() == 0) {
                LocalDateTime date = LocalDateTime.now();
                TaskLog bitacora = new TaskLog();
                String id_tareas = id_tarea;
                bitacora.setId_emisor(tarea.getId_emisor());
                bitacora.setNombre_emisor(tarea.getNombre_emisor());
                bitacora.setAccion("Actualizo la tarea");
                bitacora.setId_tarea(id_tarea);
                bitacora.setFecha_actualizacion(date);

                bitacoraRepository.save(bitacora);
                tareaService.updateById(id_tarea, tarea);
                Optional<Task> newTarea = tareaRepository.findById(id_tarea);
                mensaje = "Tarea " + id_tarea + " actualizada correctamente";
                return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()), mensaje, newTarea));
                    }else {mensaje = "Error de validacion de tarea a actualizar";
                return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY.value()), mensaje, validarActualizacion));
                }
            }else {
            //return ResponseEntity.ok(new Respuesta(String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY.value()),e.getMessage()));
            mensaje = "El id no fue encontrado";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()), mensaje));
        }
    }

    @DeleteMapping("/cancelarTarea/{id}") //Tareas
    public ResponseEntity<?> deleteById(@PathVariable String id){
        String mensaje;
        Optional<Task> oTarea = tareaService.findById(id);
        if (oTarea.isPresent()) {
            tareaService.deleteById(id);
            mensaje = "Tarea con id: "+id+" cancelada";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje));
        }else {
            mensaje = "Tarea con Id: "+id +" no encontrado";
            //return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
    }

    @GetMapping("/filtrarTareasPorPrioridad/{prioridad}")//Reportes
    public ResponseEntity<?> getTareasByPrioridad(@PathVariable String prioridad) {
        Iterable<Task> tareas = tareaRepository.findByPriority(prioridad);
        int nDocumentos = ((Collection<Task>) tareas).size();
        if(nDocumentos == 0){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(tareas.iterator());
    }

    @GetMapping("/filtrarTareasPorGrupo/{id_grupo}")// Reportes
    public ResponseEntity<?> getTareasByGrupo(@PathVariable String id_grupo) {
        Iterable<Task> tareas = tareaRepository.findByIdGrupo(id_grupo);
        String mensaje;
        //Si no existe el id_grupo
        int nDocumentos = ((Collection<Task>) tareas).size();
        if(nDocumentos == 0){
            //Map<String, Object> body = new LinkedHashMap<>();//LinkedHashMap conserva el orden de inserción
            Map<String, String> data = new LinkedHashMap<>();
            //body.put("statusCode", String.valueOf(HttpStatus.NOT_FOUND.value()));
            data.put("id_grupo","No hay tareas para el Grupo Id: "+ id_grupo);
            //body.put("Data",data);
            mensaje = "No hay tareas para el Grupo Id: "+ id_grupo;
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()), mensaje, data));
        }
        mensaje = "Petición con éxito";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,tareas.iterator()));
    }

    @GetMapping("/filtrarPrioridadTareasPorUsuario/{prioridad}&{idReceptor}") //Tareas
    public ResponseEntity<?> getUsuarioTareasByPrioridad(@PathVariable String prioridad,@PathVariable String idReceptor) {
        Iterable<Task> tareas = tareaRepository.findIdReceptorTareaByPrioridad(idReceptor, prioridad);
        int nDocumentos = ((Collection<Task>) tareas).size();
        String mensaje;
        if(nDocumentos == 0){
            mensaje = "No se encontró la tarea";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
        mensaje = "Se encontraron tareas";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,tareas));
    }

    @GetMapping("/obtenerTareasQueAsignoPorId/{id}")    //REPORTES
    public ResponseEntity<?> getAllTareasOutByUserId(@PathVariable String id){
        Iterable<Task> tareas =tareaRepository.getAllOutByUserId(id);
        int nDocumentos = ((Collection<Task>) tareas).size();
        String mensaje;
        if(nDocumentos == 0){
            mensaje="No se encontraron tareas";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
        mensaje = "Se encontraron tareas";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,tareas));
    }

    @GetMapping("/obtenerTareasQueLeAsignaronPorId/{id}") //REPORTES
    public ResponseEntity<?> getAllTareasInByUserId(@PathVariable String id){
        System.out.println(id);
        //Validar Id en la BD
        //Validar tareas !=0
        Iterable<Task> tareas = tareaRepository.getAllInByUserId(id);
        String mensaje;
        int nDocumentos = ((Collection<Task>) tareas).size();
        if(nDocumentos == 0){
            mensaje = "No se encontraron tareas";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
        mensaje = "Se encontraron tareas";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,tareas));
    }



    @GetMapping("/obtenerTareasPorGrupoYEmisor/{id_grupo}&{id_emisor}")    //Reportes
    public ResponseEntity<?> getAllTareasByGrupoAndIdEmisor(@PathVariable String id_grupo,@PathVariable String id_emisor){
        Iterable<Task> tareas =tareaRepository.getAllByGroupAndIdEmisor(id_grupo,id_emisor);
        String mensaje;
        int nDocumentos = ((Collection<Task>) tareas).size();
        if(nDocumentos == 0){
            mensaje = "No se encontraron tareas";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
        mensaje = "Se encontraron tareas";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,tareas));
    }

    @GetMapping("/obtenerTareasPorGrupoYReceptor/{id_grupo}&{id_receptor}") //Reportes que le fueron asignadas a un miembro inferior al Lider
    public ResponseEntity<?> getAllTareasInByUserId(@PathVariable String id_grupo, @PathVariable String id_receptor){
        //Validar id_grupo en la BD
        //Validar id_receptor en la BD

        //Validar tareas !=0
        Iterable<Task> tareas = tareaRepository.getAllByGroupAndIdReceptor(id_grupo,id_receptor);
        String mensaje;
        int nDocumentos = ((Collection<Task>) tareas).size();
        if(nDocumentos == 0){
            mensaje = "No se encontraron tareas";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
        mensaje = "Se encontraron tareas";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje,tareas));
    }

    @PutMapping("/actulizarEstatus/{id_tarea}&{estatus}")   // Tareas
    public ResponseEntity<?> actualizarEstatus(@PathVariable String id_tarea, @PathVariable String estatus){
        //Validar Id tarea
        //Validar estatus
        String mensaje = "";
        Optional<Task> oTarea = tareaService.findById(id_tarea);
        if (oTarea.isPresent()) {
            tareaService.actualizarEstatus(id_tarea, estatus);
            mensaje = "Estatus actualizado correctamente";
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje));
        }else{
            mensaje = "Id no encontrado";
            //return ResponseEntity.ok(new Respuesta(String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY.value()),e.getMessage()));
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()), mensaje));
        }

        }
    @PutMapping("/actualizarLeidoPorIdTarea/{id_tarea}")    //Tarea
    public ResponseEntity<?> actualizaLeidoPorIdTarea(@PathVariable String id_tarea){
        String mensaje;
        Optional<Task> oTarea = tareaService.findById(id_tarea);
        if (oTarea.isPresent()) {
            tareaService.actualizaLeido(id_tarea, true);
            mensaje = "Actualizacion Leído de tarea con id " + id_tarea;
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()),mensaje));
        }else{
            mensaje = "Id no encontrado";
            //return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()),mensaje));
        }
    }

    @GetMapping("/obtenerTareasQueLeAsignaronPorIdYEstatus/{id_usuario}&{estatus}") // Tareas
    public ResponseEntity<?> obtenerTareasQueLeAsignaronPorIdYEstatus(@PathVariable String id_usuario,@PathVariable String estatus){
        Iterable<Task> tareas = tareaRepository.getAllByIdReceptorAndStatus(id_usuario,estatus);
        int nDocumentos = ((Collection<Task>) tareas).size();
        String mensaje;
        if(nDocumentos==0){
            mensaje = "No hay tareas que le asignaron al id: "+id_usuario+" por estatus: "+estatus;
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()), mensaje));
        }
        mensaje = "Tareas que le asignaron al id: "+id_usuario+" por estatus: "+estatus+ " obtenidas correctamente";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()), mensaje, tareas));
    }

    @GetMapping("/obtenerTareasQueAsignoPorIdYEstatus/{id_usuario}&{estatus}")  // Tareas
    public ResponseEntity<?> obtenerTareasQueAsignoPorIdYEstatus(@PathVariable String id_usuario,@PathVariable String estatus){
        Iterable<Task> tareas = tareaRepository.getAllByIdEmisorAndStatus(id_usuario,estatus);
        String mensaje;
        int nDocumentos = ((Collection<Task>) tareas).size();
        if(nDocumentos ==0){
            mensaje = "No hay tareas que asignó el id: "+id_usuario+" por estatus: "+estatus;
            return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.NOT_FOUND.value()), mensaje));
        }
        mensaje = "Tareas que asignó el id: "+id_usuario+" por estatus: "+estatus+ " obtenidas correctamente";
        return ResponseEntity.ok(new ResponseTask(String.valueOf(HttpStatus.OK.value()), mensaje, tareas));
    }
}
